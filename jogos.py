import adivinhacao
import forca

def select_jogo():

    print("##################################################################################")
    print("#################################### JOGOS  ######################################")
    print("##################################################################################")

    print("SELECIONE O JOGO!")
    print("1-ADVINHACAO, 2-FORCA")

    jogo_select = int(input("SELECIONE O JOGO:"))

    if(jogo_select == 1):
        adivinhacao.jogar()
    else:
        forca.jogar()


if(__name__ == "__main__"):
    select_jogo()